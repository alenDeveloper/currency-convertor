package com.doggy.mvvm_dagger_hilt

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

@HiltAndroidApp
class ProjectApplication : Application()