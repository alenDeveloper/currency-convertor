package com.doggy.mvvm_dagger_hilt.main

import okhttp3.Interceptor
import okhttp3.Response

private const val API_KEY = "faf11a078814afe22387d143c5ca5452"

class ApiInterceptor : Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val url = chain.request().url.newBuilder().addQueryParameter("access_key", API_KEY).build()
        return chain.proceed(chain.request().newBuilder().url(url).build())
    }
}