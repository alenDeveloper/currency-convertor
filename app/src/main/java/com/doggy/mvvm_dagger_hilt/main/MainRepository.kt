package com.doggy.mvvm_dagger_hilt.main

import com.doggy.mvvm_dagger_hilt.data.models.CurrencyResponse
import com.doggy.mvvm_dagger_hilt.util.Resource

interface MainRepository {
    suspend fun getRates(): Resource<CurrencyResponse>
}