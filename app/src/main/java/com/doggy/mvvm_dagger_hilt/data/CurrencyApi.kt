package com.doggy.mvvm_dagger_hilt.data

import com.doggy.mvvm_dagger_hilt.data.models.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET

interface CurrencyApi {
    @GET("/latest")
    suspend fun getRates(): Response<CurrencyResponse>
}